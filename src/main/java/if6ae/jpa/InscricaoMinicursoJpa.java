/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if6ae.jpa;

import if6ae.entity.InscricaoMinicurso;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 *
 * @author VBoxUser
 */
public class InscricaoMinicursoJpa extends JpaController {

    public InscricaoMinicursoJpa() {
    }

    public List<InscricaoMinicurso> findAll() {
        EntityManager em = null;
        try {
            em = getEntityManager();
            TypedQuery<InscricaoMinicurso> q = em.createQuery(
                "select i from InscricaoMinicurso i",
                InscricaoMinicurso.class);
            return q.getResultList();
        } finally {
            if (em != null) em.close();
        }
    }
}
