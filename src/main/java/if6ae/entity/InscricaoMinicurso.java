/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package if6ae.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Wilson
 */
@Entity
@Table(name = "inscricao_minicurso")
@NamedQueries({
    @NamedQuery(name = "InscricaoMinicurso.findAll", query = "SELECT c FROM InscricaoMinicurso c")})
@IdClass(InscricaoMinicursoId.class)
public class InscricaoMinicurso implements Serializable {
    private static final long serialVersionUID = 1L;
    
    @Id
    private Integer numero_inscricao;
    
    @Id
    private Integer minicurso;
    
    @Column(name = "data_hora")
    @Temporal(TemporalType.TIMESTAMP)
    private Date data_hora;
    
    @Column(name = "situacao")
    private Integer situacao;
    
    @ManyToOne
    @JoinColumn(name="numero_inscricao", updatable = false, insertable = false, referencedColumnName="numero")
    private Inscricao objInscricao;

    @ManyToOne
    @JoinColumn(name="minicurso", updatable = false, insertable = false, referencedColumnName="codigo")
    private Minicurso objMinicurso;

    public InscricaoMinicurso() {
        
    }

    public Integer getNumero_inscricao() {
        return numero_inscricao;
    }

    public void setNumero_inscricao(Integer numero_inscricao) {
        this.numero_inscricao = numero_inscricao;
    }

    public Integer getMinicurso() {
        return minicurso;
    }

    public void setMinicurso(Integer minicurso) {
        this.minicurso = minicurso;
    }

    public Date getData_hora() {
        return data_hora;
    }

    public void setData_hora(Date data_hora) {
        this.data_hora = data_hora;
    }

    public Integer getSituacao() {
        return situacao;
    }

    public void setSituacao(Integer situacao) {
        this.situacao = situacao;
    }

    public Inscricao getObjInscricao() {
        return objInscricao;
    }

    public void setObjInscricao(Inscricao objInscricao) {
        this.objInscricao = objInscricao;
    }

    public Minicurso getObjMinicurso() {
        return objMinicurso;
    }

    public void setObjMinicurso(Minicurso objMinicurso) {
        this.objMinicurso = objMinicurso;
    }

    @Override
    public String toString() {
        return "if6ae.entity.InscricaoMinicurso[ numero_inscricao=" + numero_inscricao + " ]";
    }
    
}
