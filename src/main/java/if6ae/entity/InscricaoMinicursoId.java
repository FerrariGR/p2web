/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if6ae.entity;

import java.io.Serializable;

/**
 *
 * @author VBoxUser
 */
public class InscricaoMinicursoId implements Serializable {
    private Integer numero_inscricao;
    
    private Integer minicurso;

    public Integer getNumero_inscricao() {
        return numero_inscricao;
    }

    public void setNumero_inscricao(Integer numero_inscricao) {
        this.numero_inscricao = numero_inscricao;
    }

    public Integer getMinicurso() {
        return minicurso;
    }

    public void setMinicurso(Integer minicurso) {
        this.minicurso = minicurso;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (numero_inscricao != null ? numero_inscricao.hashCode() : 0);
        hash += (minicurso != null ? minicurso.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InscricaoMinicursoId)) {
            return false;
        }
        InscricaoMinicursoId other = (InscricaoMinicursoId) object;
        return (other.minicurso.equals(this.minicurso) && other.numero_inscricao.equals(this.numero_inscricao));
    }
}
