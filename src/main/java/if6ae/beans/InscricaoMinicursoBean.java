/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package if6ae.beans;

import if6ae.entity.InscricaoMinicurso;
import if6ae.jpa.InscricaoMinicursoJpa;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;

/**
 *
 * @author VBoxUser
 */
@ManagedBean
@RequestScoped
public class InscricaoMinicursoBean {
    public List<InscricaoMinicurso> getInscricoes() {
        return new InscricaoMinicursoJpa().findAll();
    }
}
